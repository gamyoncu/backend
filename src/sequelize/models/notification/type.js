const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");

const NotificationType = sequelize.define("notification_type", {
  id: templates.pk(),
  name: templates.text(),
});

const refNotificationTypeId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: NotificationType,
    key: "id",
  },
};

module.exports = {
  NotificationType,
  refNotificationTypeId,
};
