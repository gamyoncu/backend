const { sequelize } = require("../../init");
const { refUserId } = require("../user/list");
const { refNotificationId } = require("./list");

const NotificationOfUser = sequelize.define("notification_of_user", {
  userId: refUserId,
  notificationId: refNotificationId,
});

module.exports = {
  NotificationOfUser,
};
