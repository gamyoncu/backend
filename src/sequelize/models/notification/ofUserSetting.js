const { templates } = require("../../base");
const { sequelize } = require("../../init");
const { refUserId } = require("../user/list");

const NotificationOfUserSetting = sequelize.define(
  "notification_of_user_setting",
  {
    userId: {
      ...refUserId,
      unique: true,
    },
    enabled: templates.boolean(),
    token: templates.text(),
  }
);

module.exports = {
  NotificationOfUserSetting,
};
