const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refNotificationTypeId } = require("./type");

const Notification = sequelize.define("notification", {
  id: templates.pk(),
  typeId: refNotificationTypeId,

  title: templates.text(),
  body: templates.text(),
  data: templates.data(),
});

const refNotificationId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: Notification,
    key: "id",
  },
};

module.exports = {
  Notification,
  refNotificationId,
};
