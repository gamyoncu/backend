const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refEnumStatusId } = require("../enum/status");
const { refUserTypeId } = require("./type");

const User = sequelize.define("user", {
  id: templates.pk(),

  phone: templates.phone(),
  password: templates.password(),

  typeId: refUserTypeId,
  statusId: refEnumStatusId,
});

const refUserId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: User,
    key: "id",
  },
};

module.exports = {
  User,
  refUserId,
};
