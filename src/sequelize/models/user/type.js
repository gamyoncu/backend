const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refEnumStatusId } = require("../enum/status");

const UserType = sequelize.define("user_type", {
  id: templates.pk(),
  name: templates.text(),

  statusId: refEnumStatusId,
});

const refUserTypeId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: UserType,
    key: "id",
  },
};

module.exports = {
  UserType,
  refUserTypeId,
};
