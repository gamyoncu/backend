const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { ENUM_STATUS } = require("../../../constants/enum");

const EnumStatus = sequelize.define("enum_status", {
  id: templates.pk(),
  name: templates.text(),
});

const refEnumStatusId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  defaultValue: ENUM_STATUS.active,
  references: {
    model: EnumStatus,
    key: "id",
  },
};

module.exports = {
  EnumStatus,
  refEnumStatusId,
};
