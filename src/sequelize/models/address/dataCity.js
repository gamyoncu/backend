const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refEnumStatusId } = require("../enum/status");
const { refDataCountryId } = require("./DataCountry");
const { refDataRegionId } = require("./dataRegion");

const DataCity = sequelize.define("address_data_city", {
  id: templates.pk(),
  name: templates.text(),
  code: templates.text(),

  regionId: refDataRegionId,
  countryId: refDataCountryId,
  // status
  statusId: refEnumStatusId,
});

const refDataCityId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: DataCity,
    key: "id",
  },
};

module.exports = {
  DataCity,
  refDataCityId,
};
