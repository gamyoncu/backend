const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refEnumStatusId } = require("../enum/status");

const DataCountry = sequelize.define("address_data_country", {
  id: templates.pk(),
  name: templates.text(),
  code: templates.text(),
  // status
  statusId: refEnumStatusId,
});

const refDataCountryId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: DataCountry,
    key: "id",
  },
};

module.exports = {
  DataCountry,
  refDataCountryId,
};
