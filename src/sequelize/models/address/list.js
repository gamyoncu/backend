const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refEnumStatusId } = require("../enum/status");
const { refDataCityId } = require("./dataCity");
const { refDataRegionId } = require("./dataRegion");
const { refDataCountryId } = require("./DataCountry");

const Address = sequelize.define("address", {
  id: templates.pk(),

  address: templates.text(),
  address2: templates.text(),
  postalCode: templates.text(),
  latitude: {
    allowNull: false,
    defaultValue: 0,
    type: DataTypes.DOUBLE,
  },
  longitude: {
    allowNull: false,
    defaultValue: 0,
    type: DataTypes.DOUBLE,
  },
  cityId: refDataCityId,
  regionId: refDataRegionId,
  countryId: refDataCountryId,

  statusId: refEnumStatusId,
});

const refAddressId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: Address,
    key: "id",
  },
};

module.exports = {
  Address,
  refAddressId,
};
