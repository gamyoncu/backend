const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refEnumStatusId } = require("../enum/status");
const { refDataCountryId } = require("./DataCountry");

const DataRegion = sequelize.define("address_data_region", {
  id: templates.pk(),
  name: templates.text(),
  code: templates.text(),

  countryId: refDataCountryId,
  // status
  statusId: refEnumStatusId,
});

const refDataRegionId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: DataRegion,
    key: "id",
  },
};

module.exports = {
  DataRegion,
  refDataRegionId,
};
