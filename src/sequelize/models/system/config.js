const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");

const SysConfig = sequelize.define("sys_config", {
  id: templates.pk(),
  configKey: templates.text(),
  valueText: templates.text(),
  valueNumber: templates.number(),
  valueBoolean: templates.boolean(),
  valueJSON: templates.data(),
});

const refSysConfigId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: SysConfig,
    key: "id",
  },
};

module.exports = {
  SysConfig,
  refSysConfigId,
};
