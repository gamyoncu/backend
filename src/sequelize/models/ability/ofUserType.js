const { sequelize } = require("../../init");
const { DataTypes } = require("sequelize");
const { refUserTypeId } = require("../user/type");
const { refAbilityId, Ability } = require("./list");
const { refEnumStatusId } = require("../enum/status");

const AbilityOfUserType = sequelize.define("ability_of_role", {
  abilityId: refAbilityId,
  typeId: refUserTypeId,

  statusId: refEnumStatusId,
});

AbilityOfUserType.belongsTo(Ability);

const refAbilityOfUserTypeId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: AbilityOfUserType,
    key: "id",
  },
};

module.exports = {
  AbilityOfUserType,
  refAbilityOfUserTypeId,
};
