const { sequelize } = require("../../init");
const { DataTypes } = require("sequelize");
const { refUserId } = require("../user/list");
const { refAbilityId } = require("./list");

const AbilityOfUser = sequelize.define("ability_of_user", {
  abilityId: refAbilityId,
  userId: refUserId,
});

const refAbilityOfUserId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: AbilityOfUser,
    key: "id",
  },
};

module.exports = {
  AbilityOfUser,
  refAbilityOfUserId,
};
