const { sequelize } = require("../../init");
const { templates } = require("../../base");
const { DataTypes } = require("sequelize");
const { refEnumStatusId } = require("../enum/status");

const Ability = sequelize.define("ability", {
  id: templates.pk(),

  action: templates.text(),
  subject: templates.text(),

  statusId: refEnumStatusId,
});

const refAbilityId = {
  allowNull: false,
  type: DataTypes.INTEGER,
  references: {
    model: Ability,
    key: "id",
  },
};

module.exports = {
  Ability,
  refAbilityId,
};
