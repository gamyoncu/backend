const { sequelize } = require("./init");
const fs = require("fs");

var normalizedPath = require("path").join(__dirname, "models");

console.log("normalizedPath", normalizedPath);
fs.readdirSync(normalizedPath).forEach(function (modelName) {
  const modelPath = `${normalizedPath}/${modelName}`;
  fs.readdirSync(modelPath).forEach(function (file) {
    require(`${modelPath}/${file}`);
  });
});

module.exports = sequelize;
