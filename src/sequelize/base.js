const { DataTypes } = require("sequelize");
const { passwordHandler } = require("../utils/password");

const templates = {
  pk: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      }
    ),
  number: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      }
    ),
  text: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        defaultValue: "",
        type: DataTypes.STRING,
      }
    ),

  boolean: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.BOOLEAN,
      }
    ),
  data: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        defaultValue: {},
        type: DataTypes.JSON,
      }
    ),
  // custom
  email: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true,
        validate: {
          isEmail: true,
        },
      }
    ),
  password: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        type: DataTypes.STRING,
        set(value) {
          this.setDataValue("password", passwordHandler.hash(value));
        },
      }
    ),
  phone: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        defaultValue: "",
        type: DataTypes.STRING,
        // TODO validate
      }
    ),
  website: () =>
    Object.assign(
      {},
      {
        allowNull: false,
        defaultValue: "",
        type: DataTypes.STRING,
        // TODO validate
      }
    ),
};

module.exports = {
  templates,
};
