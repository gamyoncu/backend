function HttpResponse(dto, message = "") {
  return {
    success: true,
    error: false,
    message,
    dto,
  };
}

module.exports = HttpResponse;
