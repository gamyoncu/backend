const { Http } = require("@status/codes");

class HttpError extends Error {
  constructor(message, errorCode = Http.InternalServerError) {
    super(message);
    this.code = errorCode;
  }
}

module.exports = HttpError;
