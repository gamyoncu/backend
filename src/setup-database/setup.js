const { SYS_CONFIG } = require("../constants/config");
const { ABILITIES } = require("../constants/data/ability");
const {
  MAIL_PARSER_INTEGRATIONS,
} = require("../constants/data/mailParserIntegration");
const { ENUM_STATUS, NOTIFICATION_TYPE } = require("../constants/enum");
const { USER_TYPES } = require("../constants/type");
const sequelize = require("../sequelize");

async function reset() {
  console.log("Will rewrite database, adding some dummy data.");

  await sequelize.sync({ force: true });

  // system init
  await sequelize.models.sys_config.bulkCreate(Object.values(SYS_CONFIG));

  await sequelize.models.enum_status.bulkCreate(
    Object.keys(ENUM_STATUS).map((key) => {
      return { id: ENUM_STATUS[key], name: key };
    })
  );

  // notification
  await sequelize.models.notification_type.bulkCreate(
    Object.keys(NOTIFICATION_TYPE).map((key) => {
      return { id: NOTIFICATION_TYPE[key], name: key, statusId: 1 };
    })
  );

  // user role
  await sequelize.models.user_type.bulkCreate(
    Object.keys(USER_TYPES).map((key) => {
      return { id: USER_TYPES[key], name: key, statusId: 1 };
    })
  );

  // Mock data

  // mock users
  await sequelize.models.user.bulkCreate([
    {
      phone: `+905062732810`,
      password: "123456",
      typeId: USER_TYPES.user,
      statusId: 1,
    },
  ]);

  console.log("Done!");
}

reset();
