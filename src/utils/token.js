const jwt = require("jsonwebtoken");
const { JWT_CONFIG } = require("../constants/config");

/**
 *
 *
 * @export
 * @param {number} userId
 * @param {number} typeId
 * @return {string} token
 */
function sign(userId, typeId) {
  return jwt.sign(
    {
      userId,
      typeId,
    },
    JWT_CONFIG.secret,
    {
      expiresIn: JWT_CONFIG.expiresIn,
    }
  );
}

/**
 *
 *
 * @param {string} token
 * @return {boolean}
 */
function verify(token) {
  return jwt.verify(token, JWT_CONFIG.secret);
}

module.exports = {
  tokenHandler: {
    sign,
    verify,
  },
};
