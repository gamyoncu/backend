const bcrypt = require("bcryptjs");

function hash(password) {
  return bcrypt.hashSync(password, 12);
}

function compare(password1, password2) {
  return bcrypt.compareSync(password1, password2);
}

module.exports = {
  passwordHandler: {
    hash,
    compare,
  },
};
