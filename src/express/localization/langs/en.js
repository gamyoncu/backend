const data = {
  "Could not find this route.": "Could not find this route.",
  "An unknown error occurred!": "An unknown error occurred!",
  "Authentication failed!": "Authentication failed!",
};

module.exports = data;
