const data = {
  "Could not find this route.": "Yol bulunamadı",
  "An unknown error occurred!": "Bilinmeyen bir hata oluştu!",
  "Authentication failed!": "Giriş yapma başarısız!",
};

module.exports = data;
