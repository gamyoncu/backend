const localizify = require("localizify").default;

localizify.add("tr", require("./langs/tr")).add("en", require("./langs/en"));

function localization(req, res, next) {
  const lang = req.headers["accept-language"] || "en";
  localizify.setLocale(lang);
  next();
}

module.exports = {
  localization,
};
