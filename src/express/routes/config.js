const express = require("express");
const { models } = require("../../sequelize");
const { Http } = require("@status/codes");
const HttpError = require("../../class/httpError");
const { SYS_CONFIG } = require("../../constants/config");
const { ENUM_OS } = require("../../constants/enum");

var router = express.Router();

router.post("/mobile", async (req, res, next) => {
  const {
    body: { os, version },
  } = req;

  try {
    const sys_configVersion = await models.sys_config.findOne({
      where: {
        configKey: SYS_CONFIG.MOBILE.configKey,
      },
    });

    if (version === sys_configVersion.valueText)
      return res.status(Http.Ok).json({
        forceUpdate: false,
      });
    return res.status(Http.Ok).json({
      forceUpdate: true,
      url:
        os === ENUM_OS.ios
          ? SYS_CONFIG.APPSTORE_URL.valueText
          : SYS_CONFIG.PLAYSTORE_URL.valueText,
    });
  } catch (error) {
    console.log("error", error);
    return next(new HttpError("mobile :: error"));
  }
});

module.exports = router;
