const express = require("express");
const { check, validateMiddleware } = require("../middleware/validation");
const { verifyAllUsersToken } = require("../middleware/auth");
const { models, Op } = require("../../sequelize");
const { USER_TYPES } = require("../../constants/type");
const { Http } = require("@status/codes");
const HttpError = require("../../class/httpError");
const { tokenHandler } = require("../../utils/token");
const { passwordHandler } = require("../../utils/password");
const { ENUM_STATUS } = require("../../constants/enum");
const { Ability } = require("../../sequelize/models/ability/list");

const handleAuth = async (req, res, next) => {
  const {
    auth: { userId, typeId },
  } = req;

  // sign in token by typeId and userId
  console.log("userId", userId);
  const token = tokenHandler.sign(userId, typeId);

  try {
    // get user
    const user = await models.user.findByPk(userId);
    const ability = await models.ability_of_role.findAll({
      where: { statusId: ENUM_STATUS.active, typeId },
      include: [
        {
          model: Ability,
          required: true,
          where: { statusId: ENUM_STATUS.active },
          attributes: ["action", "subject"],
        },
      ],
    });

    return res.status(Http.Created).json({
      success: true,
      error: false,
      dto: {
        userId,
        typeId,
        token,
        ability: ability.map((a) => a.ability),
        user: {
          ...user.get(),
          password: undefined,
        },
      },
    });
  } catch (error) {
    console.log("error", error);
    return next(new HttpError("handleAuth::Error"));
  }
};

var router = express.Router();

/**
 * LoginReq
 * @typedef {object} LoginReq
 * @property {string} phone.required - phone
 * @property {string} password.required - Password
 */

/**
 *  POST /auth/login
 *  @tags Auth
 *  @param {LoginReq} request.body.required
 */
router.post(
  "/login",
  [check("phone").isMobilePhone(), check("password").isLength({ min: 6 })],
  validateMiddleware,
  async (req, res, next) => {
    const {
      body: { phone, password },
    } = req;
    try {
      var user = await models.user.findOne({
        where: {
          phone,
        },
      });

      if (!user) {
        return next(
          new HttpError("No user found with this phone", Http.Unauthorized)
        );
      }

      const passwordIsValid = passwordHandler.compare(password, user.password);

      if (!passwordIsValid)
        return next(new HttpError("Wrong password", Http.Unauthorized));

      req.auth = {
        userId: user.id,
        typeId: user.typeId,
      };
      handleAuth(req, res, next);
    } catch (error) {
      console.log("error", error);
      return next(new HttpError("login :: error"));
    }
  }
);

/**
 * SignupReq
 * @typedef {object} SignupReq
 * @property {number} typeId.required - typeId
 * @property {string} phone.required - phone
 * @property {string} password.required - Password
 */

/**
 *  POST /auth/signup
 *  @tags Auth
 *  @param {SignupReq} request.body.required
 */
router.post(
  "/signup",
  [
    check("typeId").isIn([USER_TYPES.user]),
    check("phone").isMobilePhone(),
    check("password").isLength({ min: 6 }),
  ],
  validateMiddleware,
  async (req, res, next) => {
    const {
      body: { typeId, phone, password },
    } = req;

    try {
      const user = await models.user.create({ typeId, phone, password });

      req.auth = {
        userId: user.id,
        typeId: user.typeId,
      };
      handleAuth(req, res, next);
    } catch (error) {
      if (error.original.code === "ER_DUP_ENTRY")
        return next(
          new HttpError(
            "User already exist, please try with different e-mails",
            Http.Conflict
          )
        );
      return next(new HttpError("signup::Error"));
    }
  },
  handleAuth
);

router.post("/refresh_token", verifyAllUsersToken, handleAuth);

module.exports = router;
