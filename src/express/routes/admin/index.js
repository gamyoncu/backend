const express = require("express");

var router = express.Router();

router.use("/users", require("./users"));
router.use("/vendors", require("./vendors"));

module.exports = router;
