const express = require("express");
const { Http } = require("@status/codes");
const { Op } = require("sequelize");
const { ENUM_STATUS } = require("../../../../constants/enum");
const { models } = require("../../../../sequelize");
const {
  getIdParam,
  makeHandlerAwareOfAsyncErrors,
} = require("../../../helpers");
const { check } = require("express-validator");
const { USER_TYPES } = require("../../../../constants/type");
const { validateMiddleware } = require("../../../middleware/validation");
const HttpResponse = require("../../../../class/httpResponse");
const HttpError = require("../../../../class/httpError");

var router = express.Router();

/**
 * GET /admin/crud/user/all
 * @summary getAll
 * @tags CRUD / User
 */
router.get(
  "/all",
  makeHandlerAwareOfAsyncErrors(async (req, res) => {
    const items = await models.user.findAndCountAll();
    res.status(Http.Ok).json(HttpResponse(items));
  })
);

/**
 * GET /admin/crud/user/data
 * @summary getData
 * @tags CRUD / User
 * @param {number} limit.query - limit
 * @param {number} offset.query - offset
 * @param {number} typeId.query - typeId
 * @param {number} statusId.query - statusId
 * @param {string} q.query - q
 */
router.get(
  "/data",
  makeHandlerAwareOfAsyncErrors(async (req, res) => {
    const {
      query: { limit = 15, offset = 0, typeId = "%", statusId = "%", q = "%" },
    } = req;

    const items = await models.user.findAndCountAll({
      where: {
        [Op.and]: [
          {
            typeId: {
              [Op.like]: typeId,
            },
          },
          {
            statusId: {
              [Op.like]: statusId,
            },
          },
          {
            [Op.or]: [
              {
                id: {
                  [Op.like]: q,
                },
              },
              {
                phone: {
                  [Op.like]: q,
                },
              },
            ],
          },
        ],
      },
      limit,
      offset,
      order: [["createdAt", "ASC"]],
    });
    res.status(Http.Ok).json(HttpResponse(items));
  })
);

/**
 * GET /admin/crud/user/{id}
 * @summary getById
 * @tags CRUD / User
 * @param {number} id.path - id
 */
router.get(
  "/:id",
  makeHandlerAwareOfAsyncErrors(async (req, res, next) => {
    const id = getIdParam(req);
    const item = await models.user.findByPk(id);
    if (item) {
      return res.status(Http.Ok).json(HttpResponse(item));
    } else {
      return next(new HttpError("Item Not Found", Http.NotFound));
    }
  })
);

/**
 * User
 * @typedef {object} User
 * @property {string} phone.required - phone
 * @property {string} password.required - password
 * @property {number} typeId.required - typeId
 */

/**
 * POST /admin/crud/user
 * @summary create
 * @tags CRUD / User
 * @param {User} request.body.required
 */
router.post(
  "/",
  [
    check("phone").isMobilePhone(),
    check("password").isLength({ min: 6 }),
    check("typeId").isIn(Object.values(USER_TYPES)),
  ],
  validateMiddleware,
  makeHandlerAwareOfAsyncErrors(async (req, res) => {
    await models.user.create(req.body);
    return res.status(Http.Created).json(HttpResponse(null, "create success"));
  })
);

/**
 * PUT /admin/crud/user/{id}
 * @summary update
 * @tags CRUD / User
 * @param {number} id.path - id
 * @param {User} request.body.required
 */
router.put(
  "/:id",
  [
    check("phone").isMobilePhone().optional(),
    check("password").isLength({ min: 6 }).optional(),
    check("typeId").isIn(Object.values(USER_TYPES)).optional(),
  ],
  validateMiddleware,
  makeHandlerAwareOfAsyncErrors(async (req, res) => {
    const id = getIdParam(req);
    await models.user.update(req.body, {
      where: {
        id,
      },
    });
    return res.status(Http.Ok).json(HttpResponse(null, "update success"));
  })
);

/**
 * DELETE /admin/crud/user/{id}
 * @summary remove
 * @param {number} id.path - id
 * @tags CRUD / User
 */
router.delete(
  "/:id",
  makeHandlerAwareOfAsyncErrors(async (req, res) => {
    const id = getIdParam(req);
    await models.user.update(
      {
        statusId: ENUM_STATUS.removed,
      },
      {
        where: {
          id,
        },
      }
    );
    res.status(Http.Ok).json(HttpResponse(null, "delete success"));
  })
);

module.exports = router;
