const express = require("express");
const { check, validateMiddleware } = require("../../middleware/validation");
const { models } = require("../../../sequelize");
const { Http } = require("@status/codes");
const HttpError = require("../../../class/httpError");
const { passwordHandler } = require("../../../utils/password");

var router = express.Router();

/**
 * AccountChangePhoneReq
 * @typedef {object} AccountChangePhoneReq
 * @property {string} new_phone.required - new_phone
 * @property {string} current_password.required - current_password
 */

/**
 * POST /user/account/phone
 * @summary change phone
 * @tags Account
 * @param {AccountChangePhoneReq} request.body.required
 * @security BearerAuth
 */
router.post(
  "/phone",
  [
    check("new_phone").isMobilePhone(),
    check("current_password").isLength({ min: 6 }),
  ],
  validateMiddleware,
  async (req, res, next) => {
    const {
      auth: { userId },
      body: { new_phone, current_password },
    } = req;

    console.log("userId", userId);

    try {
      const user = await models.user.findByPk(userId);

      if (!user) {
        return next(
          new HttpError("No user found with this phone", Http.Unauthorized)
        );
      }

      const passwordIsValid = passwordHandler.compare(
        current_password,
        user.password
      );

      if (!passwordIsValid)
        return next(new HttpError("Wrong password", Http.Unauthorized));

      // TODO change phone
      await models.user.update(
        {
          phone: new_phone,
        },
        {
          where: {
            id: userId,
          },
        }
      );
      return res.status(Http.Ok).json({
        message: "account :: phone :: success",
      });
    } catch (error) {
      console.log("error", error);
      return next(new HttpError("account :: phone :: error"));
    }
  }
);

/**
 * AccountChangePasswordReq
 * @typedef {object} AccountChangePasswordReq
 * @property {string} new_password.required - new_password
 * @property {string} current_password.required - current_password
 */

/**
 * POST /user/account/password
 * @summary change password
 * @tags Account
 * @param {AccountChangePasswordReq} request.body.required
 * @security BearerAuth
 */
router.post(
  "/password",
  [
    check("new_password").isLength({ min: 6 }),
    check("current_password").isLength({ min: 6 }),
  ],
  validateMiddleware,
  async (req, res, next) => {
    const {
      auth: { userId },
      body: { new_password, current_password },
    } = req;
    try {
      const user = await models.user.findByPk(userId);

      if (!user) {
        return next(
          new HttpError("No user found with this phone", Http.Unauthorized)
        );
      }

      const passwordIsValid = passwordHandler.compare(
        current_password,
        user.password
      );

      if (!passwordIsValid)
        return next(new HttpError("Wrong password", Http.Unauthorized));

      // TODO change phone
      await models.user.update(
        {
          password: new_password,
        },
        {
          where: {
            id: userId,
          },
        }
      );
      return res.status(Http.Ok).json({
        message: "account :: password :: success",
      });
    } catch (error) {
      console.log("error", error);
      return next(new HttpError("account :: password :: error"));
    }
  }
);

module.exports = router;
