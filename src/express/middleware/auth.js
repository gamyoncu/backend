const jwt = require("jsonwebtoken");
const { t } = require("localizify");
const { JWT_CONFIG } = require("../../constants/config");
const { USER_TYPES } = require("../../constants/type");
const { Http } = require("@status/codes");

const verifyToken =
  (typeId, isForAll = false) =>
  (req, res, next) => {
    // check header or url parameters or post parameters for token
    const {
      headers: { authorization },
    } = req;

    if (!authorization || !authorization.includes(" "))
      return res.status(Http.Unauthorized).send({ message: t("no_token") });

    var token = authorization.split(" ")[1];

    // verifies secret and checks exp
    jwt.verify(token, JWT_CONFIG.secret, function (error, decoded) {
      if (!decoded || error)
        return res.status(Http.Unauthorized).send({
          message: t("useless_or_expired_token"),
          error,
        });

      if (!isForAll && decoded.typeId !== typeId) {
        return res.status(Http.Unauthorized).send({
          message: t("you do not have access for this route"),
          error,
        });
      }

      // if everything is good, save to request for use in other routes
      req.auth = {
        userId: decoded.userId,
        typeId: decoded.typeId,
      };

      next();
    });
  };

const verifyAdminToken = verifyToken(USER_TYPES.admin);
const verifyAllUsersToken = verifyToken("%", true);

module.exports = {
  verifyAdminToken,
  verifyAllUsersToken,
};
