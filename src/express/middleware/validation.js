const { validationResult, check } = require("express-validator");

function validateMiddleware(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res
      .status(422)
      .json({ message: "Invalid inputs passed, please check your data" });
    // TODO localization
  } else {
    next();
  }
}

module.exports = {
  validateMiddleware,
  check,
};
