const { SERVER_CONFIG } = require("../constants/config");

const swaggerOptions = {
  info: {
    title: "Gamyoncu API with Swagger",
    version: "1.0.0",
    description: "This is Gamyoncu API Documentation",
    contact: {
      name: "Developer",
      // url: "https://example.com",
      email: "mddemircii5@gmail.com",
    },
  },
  servers: [
    {
      url: `${SERVER_CONFIG.url}:${SERVER_CONFIG.port}/${SERVER_CONFIG.api_path}/${SERVER_CONFIG.version}`, // DEBUG
    },
  ],
  security: {
    BearerAuth: {
      type: "http",
      scheme: "bearer",
    },
  },
  baseDir: __dirname,
  // Glob pattern to find your jsdoc files (multiple patterns can be added in an array)
  filesPattern: ["./routes/*.js", "./routes/*/*.js"],
  // URL where SwaggerUI will be rendered
  swaggerUIPath: "/api-docs",
  // Expose OpenAPI UI
  exposeSwaggerUI: true,
  // Expose Open API JSON Docs documentation in `apiDocsPath` path.
  exposeApiDocs: false,
  // Open API JSON Docs endpoint.
  apiDocsPath: "/v3/api-docs",
  // Set non-required fields as nullable by default
  notRequiredAsNullable: false,
  // You can customize your UI swaggerOptions.
  // you can extend swagger-ui-express config. You can checkout an example of this
  // in the `example/configuration/swaggerOptions.js`
  swaggerUiOptions: {},
};

module.exports = swaggerOptions;
