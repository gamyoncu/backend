const { Http } = require("@status/codes");
const { t } = require("localizify");
const HttpError = require("../class/httpError");
const { SERVER_CONFIG } = require("../constants/config");
const { verifyAllUsersToken, verifyAdminToken } = require("./middleware/auth");

const express = require("express");
const app = express();

const bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: "5mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "5mb",
    extended: true,
    parameterLimit: 50000,
  })
);

const cors = require("cors");
app.use(cors());

const cookieParser = require("cookie-parser");
app.use(cookieParser());

const { localization } = require("./localization");
app.use(localization);

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Accept-Language, Authorization"
  );
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE");

  next();
});

// swagger config
const expressJSDocSwagger = require("express-jsdoc-swagger");
expressJSDocSwagger(app)(require("./swagger.config"));

// routes
const API_URL = `/${SERVER_CONFIG.api_path}/${SERVER_CONFIG.version}`;
// everyone routes
app.use(`${API_URL}/auth`, require("./routes/auth"));
app.use(`${API_URL}/config`, require("./routes/config"));
// user routes
app.use(`${API_URL}/user/*`, verifyAllUsersToken);
app.use(`${API_URL}/user/account`, require("./routes/user/account"));

// admin routes
app.use(`${API_URL}/admin/*`, verifyAdminToken);
// admin crud routes
app.use(`${API_URL}/admin/users`, require("./routes/admin/users"));

// 404 - Not Found
app.use((req, res, next) => {
  const error = new HttpError(t("Could not find this route."), Http.NotFound);
  throw error;
});

// 500 - Internal Server Error
app.use((error, req, res, next) => {
  if (req.file) {
    fs.unlink(req.file.path, (err) => {
      console.log(err);
    });
  }
  if (res.headerSent) {
    return next(error);
  }
  res.status(error.code || Http.InternalServerError);
  res.json({
    success: false,
    error: true,
    message: error.message || t("An unknown error occurred!"),
  });
});

module.exports = app;
