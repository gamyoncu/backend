const app = require("./express/app");
const sequelize = require("./sequelize");
const { logger } = require("./utils/logger");
const { SERVER_CONFIG } = require("./constants/config");

const PORT = SERVER_CONFIG.port;

async function assertDatabaseConnectionOk() {
  logger.info(`Checking database connection...`);
  try {
    await sequelize.authenticate();
    logger.info("Database connection OK!");
  } catch (error) {
    logger.error("Unable to connect to the database:");
    process.exit(1);
  }
}

async function init() {
  await assertDatabaseConnectionOk();

  logger.info(`Starting Sequelize + Express example on port ${PORT}...`);

  app.listen(PORT, () => {
    logger.info(`Express server started on port localhost:${PORT}`);
  });
}

init();
