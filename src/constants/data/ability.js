const ABILITIES = {
  template: {
    id: 1,
    action: "manage",
    subject: "template",
  },
  user: {
    id: 2,
    action: "manage",
    subject: "w_user",
  },
};

module.exports = { ABILITIES };
