const DB_CONFIG = {
  dialect: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "password",
  database: "gamyoncu",
};

const SERVER_CONFIG = {
  url: "http://localhost",
  version: "v1",
  api_path: "api",
  port: 3001,
};

const JWT_CONFIG = {
  key: "gamyoncu-code",
  secret: "gamyoncu-s3cr3t",
  expiresIn: 8640000, // expires in 24 hours * 100
};

const SYS_CONFIG = {
  MOBILE: {
    configKey: "MobileVersion",
    valueText: "v1.0.0",
  },
  APPSTORE_URL: {
    configKey: "APPSTORE_URL",
    valueText: "#TODO write url",
  },
  PLAYSTORE_URL: {
    configKey: "PLAYSTORE_URL",
    valueText: "#TODO write url",
  },
};

module.exports = {
  DB_CONFIG,
  SERVER_CONFIG,
  JWT_CONFIG,
  SYS_CONFIG,
};
