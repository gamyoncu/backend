const USER_TYPES = {
  admin: 1,
  user: 2,
};

module.exports = {
  USER_TYPES,
};
