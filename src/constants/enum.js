const ENUM_STATUS = {
  active: 1,
  inactive: 2,
  banned: 3,
  removed: 4,
};

const ENUM_OS = {
  ios: 1,
  android: 2,
};

const NOTIFICATION_TYPE = {
  alert: 1,
  message: 2,
};

module.exports = {
  ENUM_STATUS,
  ENUM_OS,
  NOTIFICATION_TYPE,
};
